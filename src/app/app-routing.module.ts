import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    // {
    //     path: 'cny/home',
    //     loadChildren: './new-year/home/home.module#NewYearHomeModule'
    // },
    {
        path: 'cny/home',
        loadChildren: './new-year/lottery/lottery.module#NewYearLotteryModule'
    },
    {
        path: 'cny/result/:cityId',
        loadChildren: './new-year/result/result.module#NewYearResultModule'
    },
    // {
    //     path: 'test-qrcode',
    //     loadChildren: './new-year/test-qrcode/test-qrcode.module#TestQrcodeModule'
    // },
    {
        path: '',
        redirectTo: '/cny/home',
        pathMatch: 'full'
    },
    { path: '**', redirectTo: '/cny/home' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
