import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewYearHomeRoutingModule } from './home-routing.module';
import { NewYearHomeComponent } from './home.component';

@NgModule({
    imports: [
        CommonModule,
        NewYearHomeRoutingModule,
    ],
    declarations: [NewYearHomeComponent]
})
export class NewYearHomeModule { }
