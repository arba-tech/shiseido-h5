import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { WXService, UtilService } from '../../services';
import { SHARE_DATA } from 'src/app/config/constants';
import * as $ from 'jquery';
declare let gtag: any;

@Component({
    selector: 'app-new-year-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class NewYearHomeComponent implements OnInit {
    cityId: number;
    utm_source: string;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private wxService: WXService,
        private utilService: UtilService
    ) {
    }

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.utm_source = params['utm_source'] ? params['utm_source'] : null;
        });
        $(document).ready(() => { // 在文档加载完毕后执行
            if (this.utilService.isWeiXin()) {
                this.wxInit();
            }
        });
    }

    wxInit() {
        this.wxService.config(window.location.href, ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone'], SHARE_DATA.home)
            .subscribe(res => {
                console.log(res);
            }, error => {
                console.log(error);
            });
    }

    toLottery() {
        $('#bgMusic').trigger('play');
        gtag('event', 'unlock');
        this.router.navigate(['cny/lottery'], { queryParams: { utm_source: this.utm_source } });
    }
}
