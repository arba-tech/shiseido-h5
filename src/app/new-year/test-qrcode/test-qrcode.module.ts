import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestQrcodeRoutingModule } from './test-qrcode-routing.module';
import { TestQrcodeComponent } from './test-qrcode.component';

@NgModule({
    imports: [
        CommonModule,
        TestQrcodeRoutingModule,
    ],
    declarations: [TestQrcodeComponent]
})
export class TestQrcodeModule { }
