import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestQrcodeComponent } from './test-qrcode.component';

const routes: Routes = [
  {
    path: '',
    component: TestQrcodeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestQrcodeRoutingModule { }
