import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { DataService, WXService, UtilService, GlobalService } from '../../services';
import { SHARE_DATA } from 'src/app/config/constants';
import * as $ from 'jquery';
declare let Shake: any;
declare let Parabola: any;
declare let gtag: any;

@Component({
    selector: 'app-new-year-lottery',
    templateUrl: './lottery.component.html',
    styleUrls: ['./lottery.component.scss']
})
export class NewYearLotteryComponent implements OnInit, OnDestroy {
    index = 0;
    shakeTime = 0;
    shakeStream: Subject<number> = new Subject<number>();
    myShakeEvent: any;
    // plane: any;
    cityId = 0;
    selCityId = 0;
    randomNum: number;
    isShake = false;
    showLotteryPoetry = false;
    showStores = 0;
    stores: [any];
    utm_source: string;
    myTimeout: any;

    itemGroup = [
        [
            {
                label: '东京',
                value: 1
            },
            {
                label: '大阪',
                value: 2
            },
            {
                label: '名古屋',
                value: 3
            },
            {
                label: '京都',
                value: 4
            },
            {
                label: '横滨',
                value: 5
            }
        ]
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private dataService: DataService,
        private wxService: WXService,
        private utilService: UtilService,
        private globalService: GlobalService
    ) {
        if (this.globalService.cityId) {
            this.cityId = Number(this.globalService.cityId);
            this.selCityId = this.cityId;
            this.toShowLotteryPoetryBox();
            try {
                let audio: any = document.getElementById('bgMusic');
                audio.pause();
                setTimeout(() => {
                    if (this.myShakeEvent) {
                        this.myShakeEvent.stop();
                    }
                }, 1000);
            } catch (error) {

            }
        }
    }

    ngOnInit() {
        this.shakeStream
            .pipe(debounceTime(500))
            .subscribe(value => {
                // 停止摇动
                this.shakeTime++;
                this.myShakeEvent.stop();
                this.showGif();
            });

        this.myShakeEvent = new Shake({
            threshold: 2
        });
        this.myShakeEvent.start();
        window.addEventListener('shake', () => {
            if (!this.isShake) {
                this.isShake = true;
                this.playAudio();
            }
            // 摇动中
            this.index++;
            this.shakeStream.next(this.index);
        }, false);
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.utm_source = params['utm_source'] ? params['utm_source'] : null;
        });
        $(document).ready(() => { // 在文档加载完毕后执行
            if (this.utilService.isWeiXin()) {
                this.wxInit(SHARE_DATA.lottery);
            }
            // $('#bgMusic').attr('src', './assets/images/5018.wav');
        });
    }

    ngOnDestroy() {
        try {
            this.showLotteryPoetry = false;
            this.globalService.cityId = null;
            if (this.myShakeEvent) {
                this.myShakeEvent.stop();
            }
            // if (this.plane) {
            //     this.plane.stop();
            // }
            let audio: any = document.getElementById('bgMusic');
            audio.pause();
            if (this.myTimeout) {
                clearInterval(this.myTimeout);
            }
        } catch (error) {

        }
    }

    wxInit(shareDate: any) {
        this.wxService.config(window.location.href, ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone'], shareDate)
            .subscribe(res => {
                console.log(res);
            }, error => {
                console.log(error);
            });
    }

    // initParabola() {
    //     $('#plane-box').removeAttr('style');
    //     this.plane = new Parabola({
    //         el: '#plane-box',
    //         offset: [0, 0],
    //         curvature: 0.005,
    //         duration: 1500,
    //         callback: () => {
    //             $('#plane-box').css({
    //                 display: 'none',
    //             });
    //             this.plane.reset();
    //             setTimeout(() => {
    //                 $('#plane-box').css({
    //                     display: 'block',
    //                 });
    //                 this.plane.start();
    //             }, 1500);
    //         }
    //     });
    //     this.plane.setOptions({
    //         targetEl: $('#lucky-bag'),
    //         curvature: 0.005,
    //         duration: 1500
    //     });
    //     this.plane.start();
    // }

    toShowLotteryPoetryBox() {
        this.showLotteryPoetry = true;
        setTimeout(() => {
            this.pointerTip();
        }, 1000);
        // setTimeout(() => {
        //     $('#plane-box').removeAttr('style');
        //     if (this.plane) {
        //         this.plane.stop();
        //     }
        //     this.initParabola();
        // }, 3000);
    }

    playAudio() {
        $('#bgMusic').trigger('play');
    }

    toShare() {
        try {
            if (this.myShakeEvent) {
                this.myShakeEvent.stop();
            }
            if (!this.isShake) {
                this.isShake = true;
                this.playAudio();
                this.showGif();
            }
        } catch (error) {

        }
    }

    showGif() {
        gtag('event', 'shake');
        // setTimeout(() => {
        //     this.isShake = false;
        // }, 2500);
        // setTimeout(() => {
        //     $('#tag-absolute').addClass('tag-absolute');
        // }, 2900);
        // setTimeout(() => {
        //     $('#tag-absolute').removeClass('tag-absolute');
        //     this.randomCityId();
        //     this.showLotteryPoetry = true;
        // }, 3800);
        // setTimeout(() => {
        //     this.initParabola();
        // }, 4000);
        setTimeout(() => {
            this.isShake = false;
            this.randomCityId();
            this.showLotteryPoetry = true;
            // if (this.plane) {
            //     this.plane.stop();
            // }
        }, 2500);
        setTimeout(() => {
            this.pointerTip();
        }, 3500);

        // setTimeout(() => {
        //     if (this.plane) {
        //         this.plane.stop();
        //     }
        //     $('#plane-box').removeAttr('style');
        //     this.initParabola();
        // }, 4000);
    }

    pointerTip() {
        try {
            if (!this.showLotteryPoetry) {
                clearTimeout(this.myTimeout);
                return;
            }
            let bag: any;
            let bagLeft: number;
            let bagTop: number;
            bag = $('.lucky-bag');
            if (!this.showLotteryPoetry) {
                return;
            }
            bagLeft = bag.position().left;
            bagTop = bag.position().top;
            $('#pointer-tag').css({
                display: 'block',
                left: `${bagLeft + 35}px`,
                top: `${bagTop + 80}px`
            });
            setTimeout(() => {
                if (!this.showLotteryPoetry) {
                    return;
                }
                bag = $('.position-box');
                bagLeft = bag.position().left;
                bagTop = bag.position().top;
                $('#pointer-tag').css({
                    left: `${bagLeft - 11}px`,
                    top: `${bagTop + 30}px`
                });
            }, 2000);
            setTimeout(() => {
                $('#pointer-tag').css({
                    display: 'none',
                });
            }, 4000);
            this.myTimeout = setTimeout(() => {
                this.pointerTip();
            }, 5000);
        } catch (error) {

        }

    }

    backShare() {
        gtag('event', 'shake-again');
        this.showLotteryPoetry = false;
        // this.plane.stop();
        // $('#plane-box').removeAttr('style');
        this.myShakeEvent.start();
        if (this.utilService.isWeiXin()) {
            this.wxInit(SHARE_DATA.lottery);
        }
        if (this.myTimeout) {
            clearInterval(this.myTimeout);
        }
    }

    closeStore() {
        this.showStores = 2;
        gtag('event', 'close-store');
        if (this.utilService.isWeiXin()) {
            this.wxInit(SHARE_DATA.citys[this.randomNum]);
        }
    }

    cityChange(e: any) {
        // console.log(e.value);
        gtag('event', 'change-city');
        this.getStoresByCityId(e.value);
    }

    getStoresByCityId(cityId: number) {
        this.dataService.getStoresByCityId(cityId)
            .subscribe(res => {
                // console.log(res);
                if (res.status === 0) {
                    this.stores = res.stores;
                    this.stores.forEach(store => {
                        if (store.openhours) {
                            store.openhours = store.openhours.split('</br>');
                        } else {
                            let openhours = [];
                            openhours.push(`${store.startTime} ~ ${store.endTime}`);
                            if (store.holidayTime) {
                                openhours.push(`${store.startTime} ~ ${store.holidayTime}`);
                            }
                            store.openhours = openhours;
                        }
                    });
                }
            }, error => {
                // console.log(error);
            });
    }

    randomCityId() {
        let num = Math.floor(Math.random() * 5);
        if (this.randomNum === num) {
            this.randomCityId();
            return;
        }
        this.randomNum = num;
        this.cityId = this.itemGroup[0][num].value;
        if (this.utilService.isWeiXin()) {
            this.wxInit(SHARE_DATA.citys[this.randomNum]);
        }
        // console.log(this.randomNum);
        // console.log(this.cityId);
        // this.cityId = 1;
        // console.log(this.cityId);
    }

    toStores() {
        gtag('event', 'open-store');
        this.showStores = 1;
        this.selCityId = this.cityId;
        // console.log(this.cityId);
        // console.log(this.selCityId);
        this.getStoresByCityId(this.selCityId);
        if (this.utilService.isWeiXin()) {
            this.wxInit(SHARE_DATA.lottery);
        }
    }

    toResult() {
        gtag('event', 'open-gift');
        this.router.navigate(['/cny/result', this.cityId], { queryParams: { utm_source: this.utm_source } });
    }
}
