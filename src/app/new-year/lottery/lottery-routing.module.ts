import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewYearLotteryComponent } from './lottery.component';

const routes: Routes = [
  {
    path: '',
    component: NewYearLotteryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewYearLotteryRoutingModule { }
