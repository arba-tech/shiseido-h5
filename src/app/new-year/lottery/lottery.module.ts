import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NewYearLotteryRoutingModule } from './lottery-routing.module';
import { NewYearLotteryComponent } from './lottery.component';
import { WeUiModule } from 'ngx-weui';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        WeUiModule.forRoot(),
        NewYearLotteryRoutingModule,
    ],
    declarations: [NewYearLotteryComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class NewYearLotteryModule { }
