import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewYearResultComponent } from './result.component';

const routes: Routes = [
  {
    path: '',
    component: NewYearResultComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewYearResultRoutingModule { }
