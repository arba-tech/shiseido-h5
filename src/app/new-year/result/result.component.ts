import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { WXService, UtilService, GlobalService } from '../../services';
import { SHARE_DATA } from 'src/app/config/constants';
import * as $ from 'jquery';
declare let gtag: any;

@Component({
    selector: 'app-new-year-result',
    templateUrl: './result.component.html',
    styleUrls: ['./result.component.scss']
})
export class NewYearResultComponent implements OnInit {
    cityId: number;
    showPrize = false;
    showRule = false;
    utm_source: string;
    qrcode = '1.jpg';

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private wxService: WXService,
        private utilService: UtilService,
        private globalService: GlobalService
    ) {
    }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.cityId = params['cityId'] ? params['cityId'] : null;
            this.globalService.cityId = this.cityId;
        });
        this.route.queryParams.subscribe((params: Params) => {
            this.utm_source = params['utm_source'] ? params['utm_source'] : null;
        });
        setTimeout(() => {
            switch (this.utm_source) {
                case 'CTRIP':
                    this.qrcode = `${this.cityId}-${this.utm_source}.jpg`;
                    break;
                case 'DSP-JAPAN':
                    this.qrcode = `${this.cityId}-${this.utm_source}.jpg`;
                    break;
                case 'DSP-HONGKONG':
                    this.qrcode = `${this.cityId}-${this.utm_source}.jpg`;
                    break;
                case 'DSP-SINGAPORE':
                    this.qrcode = `${this.cityId}-${this.utm_source}.jpg`;
                    break;
                case 'DSP-THAILAND':
                    this.qrcode = `${this.cityId}-${this.utm_source}.jpg`;
                    break;
                default:
                    this.qrcode = `${this.cityId}.jpg`;
                    break;
            }
        }, 1000);
        $(document).ready(() => { // 在文档加载完毕后执行
            if (this.utilService.isWeiXin()) {
                this.wxInit();
            }
        });
    }

    wxInit() {
        this.wxService.config(window.location.href, ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone'], SHARE_DATA.result)
            .subscribe(res => {
                console.log(res);
            }, error => {
                console.log(error);
            });
    }

    openQrcode() {
        this.showPrize = !this.showPrize;
        gtag('event', 'open-qrcode');
    }

    closeQrcode() {
        this.showPrize = !this.showPrize;
        gtag('event', 'close-qrcode');
    }

    saveQrcode(e) {
        gtag('event', 'save-qrcode');
    }

    openTerms() {
        this.showRule = !this.showRule;
        gtag('event', 'open-terms');
    }

    closeTerms() {
        this.showRule = !this.showRule;
        gtag('event', 'close-terms');
    }

    back() {
        gtag('event', 'close-gift');
        this.router.navigate(['/cny/home'], { queryParams: { utm_source: this.utm_source } });
    }
}
