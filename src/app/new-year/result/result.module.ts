import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewYearResultRoutingModule } from './result-routing.module';
import { NewYearResultComponent } from './result.component';

@NgModule({
    imports: [
        CommonModule,
        NewYearResultRoutingModule,
    ],
    declarations: [NewYearResultComponent]
})
export class NewYearResultModule { }
