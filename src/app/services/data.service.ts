import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ROUTERS } from '../config/constants';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    constructor(
        private http: HttpClient
    ) {
    }

    getStoresByCityId(cityId: number): Observable<any> {
        return new Observable(observer => {
            // let headers = new HttpHeaders({ 'OPENID': '123456', 'SESSIONKEY': '123456', 'TOKEN': '123456' });
            this.http.post(ROUTERS.getStoresByCityId, { cityId: cityId })
                .subscribe(res => {
                    observer.next(res);
                }, error => {
                    observer.error(error);
                });
        });
    }

    // testGet(): Observable<any> {
    //     return new Observable(observer => {
    //         this.http.get(`${API_SERVE_URL}/co/getco`, {})
    //             .subscribe(res => {
    //                 observer.next(res);
    //             }, error => {
    //                 observer.error(error);
    //             });
    //     });
    // }

    // testPost(): Observable<any> {
    //     return new Observable(observer => {
    //         this.http.post(`${API_SERVE_URL}/co/postco`, {})
    //             .subscribe(res => {
    //                 observer.next(res);
    //             }, error => {
    //                 observer.error(error);
    //             });
    //     });
    // }
}
