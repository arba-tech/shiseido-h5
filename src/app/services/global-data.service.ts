import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class GlobalService {
    private _cityId: string;


    constructor(
    ) {
    }

    get cityId(): any {
        return this._cityId;
    }

    set cityId(value: any) {
        this._cityId = value;
    }
}
