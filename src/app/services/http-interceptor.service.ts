import { Injectable } from '@angular/core';
import { HttpHandler, HttpHeaderResponse, HttpInterceptor, HttpProgressEvent, HttpRequest, HttpResponse, HttpSentEvent, HttpUserEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

    constructor(
    ) {
    }

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
        // console.log('interceptor');
        let authReq = req.clone({
            setHeaders: {
                // 'content-type': 'application/json; charset=utf-8'
                // 'Authorization': `Bearer 12345678`
            },
            // withCredentials: true
        });
        // console.log(authReq);
        // if (this.globalData.token) {
        //     console.log(this.globalData.token);
        // }

        return next
            .handle(authReq)
            .pipe(
                tap((event: any) => {
                }),
                catchError((res: HttpResponse<any>) => {
                    // 以错误的形式结束本次请求
                    return Observable.create((observer: any) => observer.error(res));
                })
            );
    }
}
