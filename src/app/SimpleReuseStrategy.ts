import { Router, ActivatedRouteSnapshot, DetachedRouteHandle, RouteReuseStrategy } from '@angular/router';

export class SimpleReuseStrategy implements RouteReuseStrategy {

    public static handlers: { [key: string]: DetachedRouteHandle } = {};

    private static waitDelete: string;
    // 删除快照
    public static deleteRouteSnapshot(name: string): void {
        if (SimpleReuseStrategy.handlers[name]) {
            delete SimpleReuseStrategy.handlers[name];
        } else {
            SimpleReuseStrategy.waitDelete = name;
        }
    }
    // 是否缓存[离开路由时触发]
    public shouldDetach(route: ActivatedRouteSnapshot): boolean {
        // console.log(route);
        if (route['_routerState'].url.indexOf('/cny/result/') > -1) {
            return false;
        } else {
            return true;
        }
    }

    // 缓存组件[离开路由时触发]
    public store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
        if (SimpleReuseStrategy.waitDelete && SimpleReuseStrategy.waitDelete === this.getRouteUrl(route)) {
            // 如果待删除是当前路由则不存储快照
            SimpleReuseStrategy.waitDelete = null;
            return;
        }
        SimpleReuseStrategy.handlers[this.getRouteUrl(route)] = handle;
    }

    // 是否还原[进入路由时触发]
    public shouldAttach(route: ActivatedRouteSnapshot): boolean {
        console.log(route);
        // tslint:disable-next-line:max-line-length
        // if (route['_routerState'].url.indexOf('/cny/result/') > -1) {
        //     return SimpleReuseStrategy.handlers[this.getRouteUrl(route)] = null;
        // }
        return !!SimpleReuseStrategy.handlers[this.getRouteUrl(route)];
    }

    // 还原路由[进入路由时触发], 从缓存中获取快照，若无则返回nul
    public retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
        // console.log(route);
        if (!route.routeConfig) {
            return null;
        }

        return SimpleReuseStrategy.handlers[this.getRouteUrl(route)];
    }

    // 是否复用路由[进入路由时触发]
    public shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
        return future.routeConfig === curr.routeConfig &&
            JSON.stringify(future.params) === JSON.stringify(curr.params);
    }
    // 拿到当前路由
    private getRouteUrl(route: ActivatedRouteSnapshot) {
        return route['_routerState'].url.replace(/\//g, '_');
    }
}
