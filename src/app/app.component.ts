import { Component, OnInit } from '@angular/core';
import { NavigationStart, NavigationEnd, Router, ActivatedRoute } from '@angular/router';
import { LazyService } from 'nu-lazy';
import { UtilService } from './services/util.service';
import { ToastService } from 'ngx-weui';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private srv: LazyService,
        private utilService: UtilService,
        private toastService: ToastService
    ) {
        this.router.events.subscribe((event: any) => {
            if (this.route.firstChild) {

            }
            if (event instanceof NavigationStart) {
                this.toastService.loading('加载中...');
                // console.log(event);
            }
            if (event instanceof NavigationEnd) {
                this.toastService.destroyAll();
                // console.log(event);
            }
        });
    }

    ngOnInit() {
        if (this.utilService.isWeiXin()) {
            this.loadJSSDK();
        }
    }

    async loadJSSDK() {
        let res: any = await this.srv.load(`http://res2.wx.qq.com/open/js/jweixin-1.4.0.js`);
        console.log(`JSSDK loaded:`, res[0].loaded);
    }
}
