export const HOST = 'http://wechat.arbachina.com/shiseido_cny_share';
export const WEB_LINK = 'http://wechat.arbachina.com/shiseido-cny';
export const ROUTERS = {
    getStoresByCityId: `${HOST}/store/stores`,
    wxInit: `${HOST}/jsapi/init`
};
export const SHARE_DATA = {
    home: {
        title: '快来解锁福地，新年你有“福”了！', // 分享标题
        desc: '快来测测你的新年福地，还有惊喜大奖等你赢~', // 分享描述
        link: `${WEB_LINK}/cny/home?utm_source=wechat-share`, // 分享链接
        imgUrl: `${WEB_LINK}/assets/images/share.jpg` // 分享图片
    },
    lottery: {
        title: '快来解锁福地，新年你有“福”了！', // 分享标题
        desc: '快来测测你的新年福地，还有惊喜大奖等你赢~', // 分享描述
        link: `${WEB_LINK}/cny/home?utm_source=wechat-share`, // 分享链接
        imgUrl: `${WEB_LINK}/assets/images/share.jpg` // 分享图片
    },
    result: {
        title: '新年，别错过这支变美福袋！', // 分享标题
        desc: '快来测测你的新年福地，还有惊喜大奖等你赢~', // 分享描述
        link: `${WEB_LINK}/cny/home?utm_source=wechat-share`, // 分享链接
        imgUrl: `${WEB_LINK}/assets/images/share.jpg` // 分享图片
    },
    citys: [
        {
            title: '新年来这里，瞬间解锁灵感源泉！', // 分享标题
            desc: '快来测测你的新年福地，还有惊喜大奖等你赢~', // 分享描述
            link: `${WEB_LINK}/cny/home?utm_source=wechat-share`, // 分享链接
            imgUrl: `${WEB_LINK}/assets/images/share.jpg` // 分享图片
        },
        {
            title: '这支福地签，暴露我的会玩属性', // 分享标题
            desc: '快来测测你的新年福地，还有惊喜大奖等你赢~', // 分享描述
            link: `${WEB_LINK}/cny/home?utm_source=wechat-share`, // 分享链接
            imgUrl: `${WEB_LINK}/assets/images/share.jpg` // 分享图片
        },
        {
            title: '你有支亲子福地签尚待认领', // 分享标题
            desc: '快来测测你的新年福地，还有惊喜大奖等你赢~', // 分享描述
            link: `${WEB_LINK}/cny/home?utm_source=wechat-share`, // 分享链接
            imgUrl: `${WEB_LINK}/assets/images/share.jpg` // 分享图片
        },
        {
            title: '我的朋克养生福地，竟是这里', // 分享标题
            desc: '快来测测你的新年福地，还有惊喜大奖等你赢~', // 分享描述
            link: `${WEB_LINK}/cny/home?utm_source=wechat-share`, // 分享链接
            imgUrl: `${WEB_LINK}/assets/images/share.jpg` // 分享图片
        },
        {
            title: '桃花福地！听说去这儿能花式被撩', // 分享标题
            desc: '快来测测你的新年福地，还有惊喜大奖等你赢~', // 分享描述
            link: `${WEB_LINK}/cny/home?utm_source=wechat-share`, // 分享链接
            imgUrl: `${WEB_LINK}/assets/images/share.jpg` // 分享图片
        }
    ]
};
